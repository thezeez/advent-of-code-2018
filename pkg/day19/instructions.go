package day19

type Instruction func(*Machine, int, int, int)

// addr (add register) stores into register C the result of adding register A and register B.
func (m *Machine) Addr(a, b, c int) {
	m.Registers[c] = m.Registers[a] + m.Registers[b]
}

// addi (add immediate) stores into register C the result of adding register A and value B.
func (m *Machine) Addi(a, b, c int) {
	m.Registers[c] = m.Registers[a] + b
}

// mulr (multiply register) stores into register C the result of multiplying register A and register B.
func (m *Machine) Mulr(a, b, c int) {
	m.Registers[c] = m.Registers[a] * m.Registers[b]
}

// muli (multiply immediate) stores into register C the result of multiplying register A and value B.
func (m *Machine) Muli(a, b, c int) {
	m.Registers[c] = m.Registers[a] * b
}

// banr (bitwise AND register) stores into register C the result of the bitwise AND of register A and register B.
func (m *Machine) Banr(a, b, c int) {
	m.Registers[c] = (m.Registers[a]) & (m.Registers[b])
}

// bani (bitwise AND immediate) stores into register C the result of the bitwise AND of register A and value B.
func (m *Machine) Bani(a, b, c int) {
	m.Registers[c] = (m.Registers[a]) & b
}

// borr (bitwise OR register) stores into register C the result of the bitwise OR of register A and register B.
func (m *Machine) Borr(a, b, c int) {
	m.Registers[c] = (m.Registers[a]) | (m.Registers[b])
}

// bori (bitwise OR immediate) stores into register C the result of the bitwise OR of register A and value B.
func (m *Machine) Bori(a, b, c int) {
	m.Registers[c] = (m.Registers[a]) | b
}

// setr (set register) copies the contents of register A into register C. (Input B is ignored.)
func (m *Machine) Setr(a, b, c int) {
	m.Registers[c] = m.Registers[a]
}

// seti (set immediate) stores value A into register C. (Input B is ignored.)
func (m *Machine) Seti(a, b, c int) {
	m.Registers[c] = a
}

// gtir (greater-than immediate/register) sets register C to 1 if value A is greater than register B. Otherwise,
// register C is set to 0.
func (m *Machine) Gtir(a, b, c int) {
	if a > m.Registers[b] {
		m.Registers[c] = 1
	} else {
		m.Registers[c] = 0
	}
}

// gtri (greater-than register/immediate) sets register C to 1 if register A is greater than value B. Otherwise,
// register C is set to 0.
func (m *Machine) Gtri(a, b, c int) {
	if m.Registers[a] > b {
		m.Registers[c] = 1
	} else {
		m.Registers[c] = 0
	}
}

// gtrr (greater-than register/register) sets register C to 1 if register A is greater than register B. Otherwise,
// register C is set to 0.
func (m *Machine) Gtrr(a, b, c int) {
	if m.Registers[a] > m.Registers[b] {
		m.Registers[c] = 1
	} else {
		m.Registers[c] = 0
	}
}

// eqir (equal immediate/register) sets register C to 1 if value A is equal to register B. Otherwise, register C is set
// to 0.
func (m *Machine) Eqir(a, b, c int) {
	if a == m.Registers[b] {
		m.Registers[c] = 1
	} else {
		m.Registers[c] = 0
	}
}

// eqri (equal register/immediate) sets register C to 1 if register A is equal to value B. Otherwise, register C is set
// to 0.
func (m *Machine) Eqri(a, b, c int) {
	if m.Registers[a] == b {
		m.Registers[c] = 1
	} else {
		m.Registers[c] = 0
	}
}

// eqrr (equal register/register) sets register C to 1 if register A is equal to register B. Otherwise, register C is
// set to 0.
func (m *Machine) Eqrr(a, b, c int) {
	if m.Registers[a] == m.Registers[b] {
		m.Registers[c] = 1
	} else {
		m.Registers[c] = 0
	}
}
