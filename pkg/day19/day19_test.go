package day19_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day19"
)

func TestBackgroundProcess(t *testing.T) {
	tests := []struct {
		in       []string
		shortcut bool
		out      int
	}{
		{
			[]string{
				"#ip 0",
				"seti 5 0 1",
				"seti 6 0 2",
				"addi 0 1 0",
				"addr 1 2 3",
				"setr 1 0 0",
				"seti 8 0 4",
				"seti 9 0 5",
			},
			false,
			6,
		},
		{
			[]string{
				"#ip 1",
				"addi 1 16 1",
				"seti 1 4 5 ",
				"seti 1 4 2 ",
				"mulr 5 2 4 ",
				"eqrr 4 3 4 ",
				"addr 4 1 1 ",
				"addi 1 1 1 ",
				"addr 5 0 0 ",
				"addi 2 1 2 ",
				"gtrr 2 3 4 ",
				"addr 1 4 1 ",
				"seti 2 6 1 ",
				"addi 5 1 5 ",
				"gtrr 5 3 4 ",
				"addr 4 1 1 ",
				"seti 1 7 1 ",
				"mulr 1 1 1 ",
				"addi 3 2 3 ",
				"mulr 3 3 3 ",
				"mulr 1 3 3 ",
				"muli 3 11 3",
				"addi 4 3 4 ",
				"mulr 4 1 4 ",
				"addi 4 18 4",
				"addr 3 4 3 ",
				"addr 1 0 1 ",
				"seti 0 7 1 ",
				"setr 1 4 4 ",
				"mulr 4 1 4 ",
				"addr 1 4 4 ",
				"mulr 1 4 4 ",
				"muli 4 14 4",
				"mulr 4 1 4 ",
				"addr 3 4 3 ",
				"seti 0 0 0 ",
				"seti 0 1 1 ",
			},
			true,
			2160,
		},
	}

	for i, test := range tests {
		actual := day19.BackgroundProcess(test.in, false, test.shortcut)
		if actual != test.out {
			t.Errorf("BackgroundProcess(#%d, false, %t) => %d, want %d", i, test.shortcut, actual, test.out)
		}
	}
}
