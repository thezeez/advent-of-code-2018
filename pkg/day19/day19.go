package day19

import "fmt"

type Machine struct {
	Registers  [6]int
	Ip         int
	IpRegister int
	Lines      []Program
}

func (m *Machine) Do(opcode, a, b, c int) {
	OpcodeInstructions[opcode](m, a, b, c)
}

type Program struct {
	Opcode      int
	Instruction Instruction
	A           int
	B           int
	C           int
}

//noinspection SpellCheckingInspection,GoUnusedConst
const (
	OpcodeAddr = iota
	OpcodeAddi
	OpcodeMulr
	OpcodeMuli
	OpcodeBanr
	OpcodeBani
	OpcodeBorr
	OpcodeBori
	OpcodeSetr
	OpcodeSeti
	OpcodeGtir
	OpcodeGtri
	OpcodeGtrr
	OpcodeEqir
	OpcodeEqri
	OpcodeEqrr
	NumOpcodes
)

//noinspection SpellCheckingInspection,GoUnusedGlobalVariable
var OpcodeNames = map[int]string{
	OpcodeAddr: "addr",
	OpcodeAddi: "addi",
	OpcodeMulr: "mulr",
	OpcodeMuli: "muli",
	OpcodeBanr: "banr",
	OpcodeBani: "bani",
	OpcodeBorr: "borr",
	OpcodeBori: "bori",
	OpcodeSetr: "setr",
	OpcodeSeti: "seti",
	OpcodeGtir: "gtir",
	OpcodeGtri: "gtri",
	OpcodeGtrr: "gtrr",
	OpcodeEqir: "eqir",
	OpcodeEqri: "eqri",
	OpcodeEqrr: "eqrr",
}

var OpcodeInstructions = map[int]Instruction{
	OpcodeAddr: (*Machine).Addr,
	OpcodeAddi: (*Machine).Addi,
	OpcodeMulr: (*Machine).Mulr,
	OpcodeMuli: (*Machine).Muli,
	OpcodeBanr: (*Machine).Banr,
	OpcodeBani: (*Machine).Bani,
	OpcodeBorr: (*Machine).Borr,
	OpcodeBori: (*Machine).Bori,
	OpcodeSetr: (*Machine).Setr,
	OpcodeSeti: (*Machine).Seti,
	OpcodeGtir: (*Machine).Gtir,
	OpcodeGtri: (*Machine).Gtri,
	OpcodeGtrr: (*Machine).Gtrr,
	OpcodeEqir: (*Machine).Eqir,
	OpcodeEqri: (*Machine).Eqri,
	OpcodeEqrr: (*Machine).Eqrr,
}

//noinspection SpellCheckingInspection
var NameOpcodes = map[string]int{
	"addr": OpcodeAddr,
	"addi": OpcodeAddi,
	"mulr": OpcodeMulr,
	"muli": OpcodeMuli,
	"banr": OpcodeBanr,
	"bani": OpcodeBani,
	"borr": OpcodeBorr,
	"bori": OpcodeBori,
	"setr": OpcodeSetr,
	"seti": OpcodeSeti,
	"gtir": OpcodeGtir,
	"gtri": OpcodeGtri,
	"gtrr": OpcodeGtrr,
	"eqir": OpcodeEqir,
	"eqri": OpcodeEqri,
	"eqrr": OpcodeEqrr,
}

func (m *Machine) Run(shortcut bool) {
	for m.Ip < len(m.Lines) && m.Ip >= 0 {

		if shortcut && m.Ip == 1 {
			for i := 1; i <= m.Registers[3]; i++ {
				// It's a modulo every year, isn't it?
				if (m.Registers[3])%i == 0 {
					m.Registers[0] += i
				}
			}
			return
		}

		cur := m.Lines[m.Ip]
		m.Registers[m.IpRegister] = m.Ip
		cur.Instruction(m, cur.A, cur.B, cur.C)
		m.Ip = m.Registers[m.IpRegister] + 1
	}
}

func NewMachine(input []string) Machine {
	m := Machine{}
	for _, line := range input {
		if line[0:3] == "#ip" {
			_, err := fmt.Sscanf(line, "#ip %d", &m.IpRegister)
			if err != nil {
				panic(err)
			}
		} else {
			var op string
			program := Program{}
			_, err := fmt.Sscanf(line, "%s %d %d %d", &op, &program.A, &program.B, &program.C)
			if err != nil {
				panic(err)
			}
			if opcode, ok := NameOpcodes[op]; !ok {
				panic(fmt.Errorf("no opcode for %s", op))
			} else {
				program.Instruction = OpcodeInstructions[opcode]
				program.Opcode = opcode
			}
			m.Lines = append(m.Lines, program)
		}
	}
	return m
}

func BackgroundProcess(input []string, part2, shortcut bool) int {
	m := NewMachine(input)

	if part2 {
		m.Registers[0] = 1
	}

	m.Run(shortcut)

	return m.Registers[0]
}
