package day11_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day11"
)

func TestMakeGrid(t *testing.T) {
	tests := []struct {
		in  int
		pos day11.XY
		out int
	}{
		{8, day11.XY{3, 5}, 4,},
		{57, day11.XY{122, 79}, -5,},
		{39, day11.XY{217, 196}, 0,},
		{71, day11.XY{101, 153}, 4,},
	}

	for _, test := range tests {
		grid := day11.MakeGrid(test.in)
		actual := grid[test.pos.Y-1][test.pos.X-1]
		if actual != test.out {
			t.Errorf("MakeGrid(%d) at %v => %d, want %d", test.in, test.pos, actual, test.out)
		}
	}
}

func TestMaxPower(t *testing.T) {
	tests := []struct {
		in  int
		pos day11.XY
		power int
	}{
		{18, day11.XY{33, 45}, 29},
		{42, day11.XY{21, 61}, 30},
	}

	for _, test := range tests {
		actualPos, actualPower := day11.MaxPower(day11.MakeGrid(test.in), 3)
		if actualPos != test.pos || actualPower != test.power {
			t.Errorf("MaxPower(%d) => %v,%d, want %v,%d", test.in, actualPos, actualPower, test.pos, test.power)
		}
	}
}

func TestMaxPowerGrid(t *testing.T) {
	if testing.Short() {
		t.SkipNow()
	}

	tests := []struct {
		in   int
		pos  day11.XY
		size int
	}{
		{18, day11.XY{90, 269}, 16},
		{42, day11.XY{232, 251}, 12},
	}

	for _, test := range tests {
		actualPos, actualSize := day11.MaxPowerGrid(day11.MakeGrid(test.in))
		if actualPos != test.pos || actualSize != test.size {
			t.Errorf("MaxPowerGrid(%d) => %v,%d, want %v,%d", test.in, actualPos, actualSize, test.pos, test.size)
		}
	}
}
