package day11

import (
	"fmt"
	"runtime"
)

type XY struct {
	X, Y int
}

func (xy XY) String() string {
	return fmt.Sprintf("%d,%d", xy.X, xy.Y)
}

func MakeGrid(serial int) [][]int {
	grid := make([][]int, 300)
	for y := 1; y <= 300; y++ {
		row := make([]int, 300)
		for x := 1; x <= 300; x++ {
			row[x-1] = (((x+10)*y+serial)*(x+10))/100%10 - 5
		}
		grid[y-1] = row
	}
	return grid
}

func MaxPower(grid [][]int, size int) (XY, int) {
	var maxPower int
	var maxPowerPos XY

	for y := 0; y < 300-size+1; y++ {
		for x := 0; x < 300-size+1; x++ {
			var power int
			for i := 0; i < size; i++ {
				for j := 0; j < size; j++ {
					power += grid[y+i][x+j]
				}
			}
			if power > maxPower {
				maxPowerPos = XY{x+1, y+1}
				maxPower = power
			}
		}
	}
	return maxPowerPos, maxPower
}

func MaxPowerGrid(grid [][]int) (XY, int) {
	var maxPower int
	var maxPowerPos XY
	var maxPowerSize int

	cores := runtime.NumCPU()

	type result struct {
		pos XY
		power int
		size int
	}

	out := make(chan result, 300)
	in := make(chan int, cores)

	go func() {
		for i := 1; i <= 300; i++ {
			in <- i
		}
	}()

	for i := 0; i < cores; i++ {
		go func() {
			for size := range in {
				var r result
				r.pos, r.power = MaxPower(grid, size)
				r.size = size
				out <- r
			}
		}()
	}

	for i := 0; i < 300; i++ {
		r := <- out
		if r.power > maxPower {
			maxPower = r.power
			maxPowerPos = r.pos
			maxPowerSize = r.size
		}
	}

	return maxPowerPos, maxPowerSize
}
