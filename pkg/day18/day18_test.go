package day18_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day18"
)

func TestLumberCollection(t *testing.T) {
	tests := []struct {
		in      []string
		minutes int
		out     int
	}{
		{
			[]string{
				".#.#...|#.",
				".....#|##|",
				".|..|...#.",
				"..|#.....#",
				"#.#|||#|#|",
				"...#.||...",
				".|....|...",
				"||...#|.#|",
				"|.||||..|.",
				"...#.|..|.",
			},
			10,
			1147,
		},
	}

	for i, test := range tests {
		actual := day18.LumberCollection(test.in, test.minutes)
		if actual != test.out {
			t.Errorf("LumberCollection(#%d, %d) => %d, want %d", i, test.minutes, actual, test.out)
		}
	}
}
