package day18

import "fmt"

const mapSize = 50

type Map [mapSize][mapSize]int

const (
	KindNone       = 0
	KindOpenGround = 1 << iota
	KindTrees
	KindLumberyard
)

var RuneKinds = map[uint8]int{
	' ': KindNone,
	'.': KindOpenGround,
	'|': KindTrees,
	'#': KindLumberyard,
}

var KindRunes = map[int]rune{
	KindNone:       ' ',
	KindOpenGround: '.',
	KindTrees:      '|',
	KindLumberyard: '#',
}

func NewMap(input []string) Map {
	var m Map
	for y := 0; y < mapSize && y < len(input); y++ {
		for x := 0; x < mapSize && x < len(input[y]); x++ {
			m[y][x] = RuneKinds[input[y][x]]
		}
	}
	return m
}

func (m Map) Get(x, y int) int {
	if x < 0 || x >= mapSize || y < 0 || y >= mapSize {
		return KindNone
	}
	return m[y][x]
}

func (m Map) CountNeighbors(kind, x, y int) int {
	count := 0
	for i := -1; i <= 1; i++ {
		for j := -1; j <= 1; j++ {
			if i == 0 && j == 0 {
				continue
			}
			if m.Get(x+i, y+j)&kind != 0 {
				count++
			}
		}
	}
	return count
}

func (m Map) Count(kind int) int {
	count := 0
	for y := 0; y < mapSize; y++ {
		for x := 0; x < mapSize; x++ {
			if m[y][x]&kind != 0 {
				count++
			}
		}
	}
	return count
}

func (m Map) Print() {
	for y := 0; y < mapSize; y++ {
		if m[y][0] == KindNone {
			continue
		}
		for x := 0; x < mapSize; x++ {
			if m[y][x] == KindNone {
				continue
			}
			fmt.Printf("%c", KindRunes[m[y][x]])
		}
		fmt.Println()
	}
}

func (m Map) Tick() Map {
	var n Map
	for y := 0; y < mapSize; y++ {
		for x := 0; x < mapSize; x++ {
			switch m[y][x] {
			case KindOpenGround:
				if m.CountNeighbors(KindTrees, x, y) >= 3 {
					n[y][x] = KindTrees
				} else {
					n[y][x] = KindOpenGround
				}
			case KindTrees:
				if m.CountNeighbors(KindLumberyard, x, y) >= 3 {
					n[y][x] = KindLumberyard
				} else {
					n[y][x] = KindTrees
				}
			case KindLumberyard:
				if m.CountNeighbors(KindLumberyard, x, y) < 1 || m.CountNeighbors(KindTrees, x, y) < 1 {
					n[y][x] = KindOpenGround
				} else {
					n[y][x] = KindLumberyard
				}
			default:
				n[y][x] = m[y][x]
			}
		}
	}
	return n
}

func LumberCollection(input []string, minutes int) int {
	m := NewMap(input)
	sequence := map[Map]int{}

	for i := 0; i < minutes; i++ {
		if after, ok := sequence[m]; ok {
			length := i - after
			for j := i; (minutes-j)%length != 0; j++ {
				m = m.Tick()
			}
			return m.Count(KindTrees) * m.Count(KindLumberyard)
		}
		sequence[m] = i
		m = m.Tick()
	}
	return m.Count(KindTrees) * m.Count(KindLumberyard)
}
