package day03

import (
	"regexp"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

type Claim struct {
	Id     int
	Left   int
	Top    int
	Width  int
	Height int
}

var pattern = regexp.MustCompile(`^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)$`)

func NewClaim(input string) Claim {
	match := pattern.FindAllStringSubmatch(input, -1)
	return Claim{
		helpers2017.IntOrPanic(match[0][1]),
		helpers2017.IntOrPanic(match[0][2]),
		helpers2017.IntOrPanic(match[0][3]),
		helpers2017.IntOrPanic(match[0][4]),
		helpers2017.IntOrPanic(match[0][5]),
	}
}


func Overlap(claims []string) int {
	var claimedArea = map[int]map[int]int{}
	var overlap int
	for _, data := range claims {
		claim := NewClaim(data)
		if claimedArea[claim.Top] == nil {
			claimedArea[claim.Top] = make(map[int]int)
		}

		for top := claim.Top; top < claim.Top+claim.Height; top++ {
			if claimedArea[top] == nil {
				claimedArea[top] = make(map[int]int)
			}
			for left := claim.Left; left < claim.Left+claim.Width; left++ {
				claimedArea[top][left]++
				if claimedArea[top][left] == 2 {
					overlap++
				}
			}
		}
	}
	return overlap
}

func Intact(claims []string) int {
	var claimedArea = map[int]map[int]int{}
	var intactClaims = map[int]struct{}{}

	for _, data := range claims {
		claim := NewClaim(data)
		if claimedArea[claim.Top] == nil {
			claimedArea[claim.Top] = make(map[int]int)
		}

		var busted = map[int]struct{}{}
		good := true

		for top := claim.Top; top < claim.Top+claim.Height; top++ {
			if claimedArea[top] == nil {
				claimedArea[top] = make(map[int]int)
			}
			for left := claim.Left; left < claim.Left+claim.Width; left++ {
				if id, exist := claimedArea[top][left]; exist {
					if id > 0 {
						busted[id] = struct{}{}
						claimedArea[top][left] = -1
					}
					if good {
						good = false
						busted[claim.Id] = struct{}{}
					}
				} else {
					claimedArea[top][left] = claim.Id
				}
			}
		}

		if good {
			intactClaims[claim.Id] = struct{}{}
		}

		for id := range busted {
			delete(intactClaims, id)
		}
	}

	for id := range intactClaims {
		return id
	}
	return 0
}
