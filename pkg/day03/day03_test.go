package day03_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day03"
)

func TestNewClaim(t *testing.T) {
	tests := []struct {
		in  string
		out day03.Claim
	}{
		{"#123 @ 3,2: 5x4", day03.Claim{Id: 123, Left: 3, Top: 2, Width: 5, Height: 4}},
	}

	for _, test := range tests {
		actual := day03.NewClaim(test.in)
		if actual != test.out {
			t.Errorf("Intact(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestOverlap(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"#1 @ 1,3: 4x4",
			"#2 @ 3,1: 4x4",
			"#3 @ 5,5: 2x2",
		}, 4},
	}

	for _, test := range tests {
		actual := day03.Overlap(test.in)
		if actual != test.out {
			t.Errorf("Overlap(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestIntact(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"#1 @ 1,3: 4x4",
			"#2 @ 3,1: 4x4",
			"#3 @ 5,5: 2x2",
		}, 3},
	}

	for _, test := range tests {
		actual := day03.Intact(test.in)
		if actual != test.out {
			t.Errorf("Intact(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
