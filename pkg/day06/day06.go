package day06

import (
	"sort"
	"strings"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

type Coordinate struct {
	X    int
	Y    int
	Area int
}

func Coordinates(input, sep string) []Coordinate {
	var coords []Coordinate
	for _, line := range helpers.CleanSliceOfStrings(input, sep) {
		parts := strings.Split(line, ",")
		coords = append(coords, Coordinate{
			X: helpers2017.IntOrPanic(parts[0]),
			Y: helpers2017.IntOrPanic(parts[1]),
		})
	}
	return coords
}

func Distance(a, b Coordinate) int {
	return helpers2017.Abs(a.X-b.X) + helpers2017.Abs(a.Y-b.Y)
}

func Extremes(coords []Coordinate) (int, int, int, int) {
	var minX, minY, maxX, maxY int
	for _, coord := range coords {
		if coord.X > maxX {
			maxX = coord.X
		}
		if coord.Y > maxY {
			maxY = coord.Y
		}
		if coord.X < minX || minX == 0 {
			minX = coord.X
		}
		if coord.Y < minY || minY == 0 {
			minY = coord.Y
		}
	}
	return minX, minY, maxX, maxY
}

func LargestSafe(coords []Coordinate) int {
	var minX, minY, maxX, maxY = Extremes(coords)
	var grid = make(map[int]map[int]int)

	for i, coord := range coords {
		if grid[coord.X] == nil {
			grid[coord.X] = make(map[int]int)
		}
		grid[coord.X][coord.Y] = i
	}

	for i, coord := range coords {
		if coord.X <= minX || coord.Y <= minY || coord.X >= maxX || coord.Y >= maxY {
			coords[i].Area = -1
		}
	}

	for y := minY; y <= maxY; y++ {
		for x := minX; x <= maxX; x++ {
			if grid[x] == nil {
				grid[x] = make(map[int]int)
			}

			var distances []int
			var smallest, closest int
			for i, contender := range coords {
				d := Distance(contender, Coordinate{x, y, 0})
				distances = append(distances, d)
				if smallest == 0 || d < smallest {
					smallest = d
					closest = i
				}
			}
			sort.Ints(distances)
			if distances[0] == distances[1] {
				grid[x][y] = -1
			} else {
				grid[x][y] = closest
				if coords[closest].Area >= 0 {
					coords[closest].Area++
				}
			}
		}
	}

	var max int
	for _, coord := range coords {
		if coord.Area > max {
			max = coord.Area
		}
	}
	return max - 1
}

func LargestWithin(coords []Coordinate, limit int) int {
	var area int
	var minX, minY, maxX, maxY = Extremes(coords)

	for y := minY; y <= maxY; y++ {
		for x := minX; x <= maxX; x++ {
			var sum int
			for _, coord := range coords {
				sum += Distance(coord, Coordinate{x, y, 0})
			}
			if sum < limit {
				area++
			}
		}
	}
	return area
}
