package day06_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day06"
)

func TestExtremes(t *testing.T) {
	tests := []struct {
		in   string
		minX int
		minY int
		maxX int
		maxY int
	}{
		{"1, 1; 1, 6; 8, 3; 3, 4; 5, 5; 8, 9", 1, 1, 8, 9},
	}

	for _, test := range tests {
		actualMinX, actualMinY, actualMaxX, actualMaxY := day06.Extremes(day06.Coordinates(test.in, ";"))
		if actualMinX != test.minX || actualMinY != test.minY || actualMaxX != test.maxX || actualMaxY != test.maxY {
			t.Errorf("Extremes(%q) => %d, %d, %d, %d, want %d, %d, %d, %d", test.in, actualMinX, actualMinY, actualMaxX, actualMaxY, test.minX, test.minY, test.maxX, test.maxY)
		}
	}
}

func TestDistance(t *testing.T) {
	tests := []struct {
		A   day06.Coordinate
		B   day06.Coordinate
		out int
	}{
		{day06.Coordinate{X: -10, Y: -10}, day06.Coordinate{X: 10, Y: 10}, 40},
		{day06.Coordinate{X: 4, Y: 3}, day06.Coordinate{X: 8, Y: 3}, 4},
		{day06.Coordinate{X: 4, Y: 3}, day06.Coordinate{X: 3, Y: 4}, 2},
		{day06.Coordinate{X: 4, Y: 3}, day06.Coordinate{X: 8, Y: 9}, 10},
	}

	for _, test := range tests {
		actual := day06.Distance(test.A, test.B)
		if actual != test.out {
			t.Errorf("Distance(%+v, %+v) => %d, want %d", test.A, test.B, actual, test.out)
		}
	}
}

func TestLargestSafe(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"1, 1; 1, 6; 8, 3; 3, 4; 5, 5; 8, 9", 17},
	}

	for _, test := range tests {
		actual := day06.LargestSafe(day06.Coordinates(test.in, ";"))
		if actual != test.out {
			t.Errorf("LargestSafe(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestLargestWithin(t *testing.T) {
	tests := []struct {
		in    string
		limit int
		out   int
	}{
		{"1, 1; 1, 6; 8, 3; 3, 4; 5, 5; 8, 9", 32, 16},
	}

	for _, test := range tests {
		actual := day06.LargestWithin(day06.Coordinates(test.in, ";"), test.limit)
		if actual != test.out {
			t.Errorf("LargestWithin(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
