package day21

import (
	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day19"
)

func FewestInstructions(input []string) int {
	m := day19.NewMachine(input)

	for int(m.Ip) < len(m.Lines) && m.Ip >= 0 {
		cur := m.Lines[m.Ip]

		if cur.Opcode == day19.OpcodeEqrr && (cur.A == 0 || cur.B == 0) {
			if cur.A == 0 {
				return m.Registers[cur.B]
			} else {
				return m.Registers[cur.A]
			}
		}

		m.Registers[m.IpRegister] = m.Ip
		cur.Instruction(&m, cur.A, cur.B, cur.C)
		m.Ip = m.Registers[m.IpRegister] + 1
	}
	return -1
}

func MostInstructions(input []string) int {
	m := day19.NewMachine(input)
	var checks []int

	for int(m.Ip) < len(m.Lines) && m.Ip >= 0 {
		cur := m.Lines[m.Ip]

		if cur.Opcode == day19.OpcodeEqrr && (cur.A == 0 || cur.B == 0) {
			var reg int
			if cur.A == 0 {
				reg = cur.B
			} else {
				reg = cur.A
			}
			if helpers2017.Contains(checks, m.Registers[reg]) {
				return checks[len(checks)-1]
			} else {
				checks = append(checks, m.Registers[reg])
			}
		}

		m.Registers[m.IpRegister] = m.Ip
		cur.Instruction(&m, cur.A, cur.B, cur.C)
		m.Ip = m.Registers[m.IpRegister] + 1
	}
	return -1
}
