package day22_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day22"
)

func TestMap_GeologicIndex(t *testing.T) {
	tests := []struct {
		in  string
		x   int
		y   int
		out int
	}{
		{"depth: 510\ntarget: 10,10", 0, 0, 0},
		{"depth: 510\ntarget: 10,10", 1, 0, 16807},
		{"depth: 510\ntarget: 10,10", 0, 1, 48271},
		{"depth: 510\ntarget: 10,10", 1, 1, 145722555},
		{"depth: 510\ntarget: 10,10", 10, 10, 0},
	}

	for _, test := range tests {
		m := day22.NewMap(test.in)
		actual := m.GeologicIndex(test.x, test.y)
		if actual != test.out {
			t.Errorf("NewMaps(%q).GeologicIndex(%d,%d) => %d want %d", test.in, test.x, test.y, actual, test.out)
		}
	}
}

func TestMap_ErosionLevel(t *testing.T) {
	tests := []struct {
		in  string
		x   int
		y   int
		out int
	}{
		{"depth: 510\ntarget: 10,10", 0, 0, 510},
		{"depth: 510\ntarget: 10,10", 1, 0, 17317},
		{"depth: 510\ntarget: 10,10", 0, 1, 8415},
		{"depth: 510\ntarget: 10,10", 1, 1, 1805},
		{"depth: 510\ntarget: 10,10", 10, 10, 510},
	}

	for _, test := range tests {
		m := day22.NewMap(test.in)
		actual := m.ErosionLevel(test.x, test.y)
		if actual != test.out {
			t.Errorf("NewMaps(%q).ErosionLevel(%d,%d) => %d want %d", test.in, test.x, test.y, actual, test.out)
		}
	}
}

func TestMap_Type(t *testing.T) {
	tests := []struct {
		in  string
		x   int
		y   int
		out int
	}{
		{"depth: 510\ntarget: 10,10", 0, 0, day22.TypeRocky},
		{"depth: 510\ntarget: 10,10", 1, 0, day22.TypeWet},
		{"depth: 510\ntarget: 10,10", 0, 1, day22.TypeRocky},
		{"depth: 510\ntarget: 10,10", 1, 1, day22.TypeNarrow},
		{"depth: 510\ntarget: 10,10", 10, 10, day22.TypeRocky},
	}

	for _, test := range tests {
		m := day22.NewMap(test.in)
		actual := m.Type(test.x, test.y)
		if actual != test.out {
			t.Errorf("NewMaps(%q).Type(%d,%d) => %d want %d", test.in, test.x, test.y, actual, test.out)
		}
	}
}
