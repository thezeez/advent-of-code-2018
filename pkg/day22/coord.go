package day22

type coord struct {
	x, y int
}

func (c coord) neighbors() []coord {
	n := []coord{
		{c.x + 1, c.y},
		{c.x, c.y + 1},
	}

	if c.x > 0 {
		n = append(n, coord{c.x - 1, c.y})
	}
	if c.y > 0 {
		n = append(n, coord{c.x, c.y - 1})
	}

	return n
}
