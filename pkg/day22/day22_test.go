package day22_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day22"
)

func TestRiskLevel(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"depth: 510\ntarget: 10,10", 114},
	}

	for _, test := range tests {
		actual := day22.RiskLevel(test.in)
		if actual != test.out {
			t.Errorf("RiskLevel(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestRescue(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"depth: 510\ntarget: 10,10", 45},
	}

	for _, test := range tests {
		actual := day22.Rescue(test.in)
		if actual != test.out {
			t.Errorf("Rescue(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
