package day25

import "fmt"

func HotChocolatePortal(input []string) int {
	var constellations []Constellation

	for _, line := range input {
		var c Coordinate

		if _, err := fmt.Sscanf(line, "%d,%d,%d,%d", &c[0], &c[1], &c[2], &c[3]); err != nil {
			panic(err)
		}

		var found bool
		for _, constellation := range constellations {
			if constellation.Distance(c) <= 3 {
				constellation.Add(c)
				found = true
				break
			}
		}
		if !found {
			constellations = append(constellations, NewConstellation(c))
		}
	}

	var nc []Constellation
	for i, a := range constellations {
		merged := false

		for j, b := range constellations {
			if i == j {
				continue
			}

			if b.CelestialDistance(a) <= 3 {
				b.Merge(a)
				merged = true
				break
			}
		}

		if !merged {
			nc = append(nc, a)
		}
	}

	return len(nc)
}
