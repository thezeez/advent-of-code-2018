package day25

import (
	"math"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

type Coordinate [4]int

func (c Coordinate) Distance(b Coordinate) int {
	sum := 0
	for i := 0; i < 4; i++ {
		sum += helpers2017.Abs(c[i] - b[i])
	}
	return sum
}

type Constellation map[Coordinate]struct{}

func (c Constellation) Distance(b Coordinate) int {
	shortestDistance := math.MaxInt64

	for p := range c {
		if p.Distance(b) < shortestDistance {
			shortestDistance = p.Distance(b)
		}
	}

	return shortestDistance
}

func (c Constellation) CelestialDistance(b Constellation) int {
	shortestDistance := math.MaxInt64

	for p := range c {
		if b.Distance(p) < shortestDistance {
			shortestDistance = b.Distance(p)
		}
	}

	return shortestDistance
}

func (c Constellation) Add(b Coordinate) {
	c[b] = struct{}{}
}

func (c Constellation) Merge(b Constellation) {
	for p := range b {
		c[p] = struct{}{}
		delete(b, p)
	}
}

func NewConstellation(b Coordinate) Constellation {
	c := Constellation{b: struct{}{}}
	return c
}
