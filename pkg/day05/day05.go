package day05

import (
	"regexp"
)

func Reduction(polymer string) string {
	for i := 0; i < len(polymer)-1; {
		if polymer[i] == polymer[i+1]+32 || polymer[i] == polymer[i+1]-32 {
			polymer = polymer[:i] + polymer[i+2:]
			i--
			if i < 0 {
				i = 0
			}
		} else {
			i++
		}
	}
	return polymer
}

func BestCollapse(polymer string) string {
	polymer = Reduction(polymer)
	shortest := polymer
	for i := 'A'; i <= 'Z'; i++ {
		remainingPolymer := regexp.MustCompile("(?i)" + string(i)).ReplaceAllString(polymer, "")

		collapsed := Reduction(remainingPolymer)
		if len(collapsed) < len(shortest) {
			shortest = collapsed
		}
	}
	return shortest
}
