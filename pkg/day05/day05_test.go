package day05_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day05"
)

func TestReduction(t *testing.T) {
	tests := []struct {
		in  string
		out string
	}{
		{"aA", ""},
		{"abBA", ""},
		{"abAB", "abAB"},
		{"aabAAB", "aabAAB"},
		{"dabAcCaCBAcCcaDA", "dabCBAcaDA"},
	}

	for _, test := range tests {
		actual := day05.Reduction(test.in)
		if actual != test.out {
			t.Errorf("Reduction(%q) => %q, want %q", test.in, actual, test.out)
		}
	}
}

func TestBestCollapse(t *testing.T) {
	tests := []struct {
		in  string
		out string
	}{
		{"dabAcCaCBAcCcaDA", "daDA"},
	}

	for _, test := range tests {
		actual := day05.BestCollapse(test.in)
		if actual != test.out {
			t.Errorf("BestCollapse(%q) => %q, want %q", test.in, actual, test.out)
		}
	}
}
