package day01

import (
	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func FrequencyChange(changes []int) int {
	result := 0
	for _, change := range changes {
		result += change
	}
	return result
}

func DoubleFrequencyChange(changes []int) int {
	frequency := 0
	reached := []int{0}

	for {

		for _, change := range changes {
			frequency += change
			if helpers2017.Contains(reached, frequency) {
				return frequency
			}
			reached = append(reached, frequency)
		}
	}
}
