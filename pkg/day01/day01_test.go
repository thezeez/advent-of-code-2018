package day01_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day01"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func TestFrequencyChange(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"+1, +1, +1", 3},
		{"+1, +1, -2", 0},
		{"-1, -2, -3", -6},
	}

	for _, test := range tests {
		changes := helpers.ToSliceOfInt(test.in, ",")
		actual := day01.FrequencyChange(changes)
		if actual != test.out {
			t.Errorf("FrequencyChange(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestDoubleFrequencyChange(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"+1, -1", 0},
		{"+3, +3, +4, -2, -4", 10},
		{"-6, +3, +8, +5, -6", 5},
		{"+7, +7, -2, -7, -4", 14},
	}

	for _, test := range tests {
		changes := helpers.ToSliceOfInt(test.in, ",")
		actual := day01.DoubleFrequencyChange(changes)
		if actual != test.out {
			t.Errorf("DoubleFrequencyChange(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
