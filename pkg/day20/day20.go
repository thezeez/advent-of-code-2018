package day20

import (
	"fmt"
	"strings"
)

func DooriestRoom(regex string) int {
	var level, levelStart int
	var sum int
	var options []int
	for i, r := range regex {
		switch r {
		case '(':
			if level == 0 {
				levelStart = i
			}
			level++
		case ')':
			level--
			if level == 0 {
				sum += DooriestRoom(regex[levelStart+1 : i])
			}
		case '|':
			if level == 0 {
				options = append(options, sum)
				sum = 0
			}
		case 'N', 'E', 'S', 'W':
			if level == 0 {
				sum++
			}
		}
	}
	options = append(options, sum)
	longest := -1
	for _, s := range options {
		if s == 0 {
			return 0
		}
		if s > longest {
			longest = s
		}
	}
	return longest
}


func NumberOfDoors(regex string, min int) int {
	m := &Map{0: {0: 0}}
	Walk(regex, m, 0, 0, 0)
	var sum int
	for _, row := range *m {
		for _, col := range row {
			if col >= min {
				sum++
			}
		}
	}
	return sum
}

type Map map[int]map[int]int

func (m Map) Set(doors, x, y int) {
	if m[y] == nil {
		m[y] = make(map[int]int)
	}
	if d, ok := m[y][x]; !ok || doors < d {
		m[y][x] = doors
	}
}

func (m Map) Print() {
	var minX, minY, maxX, maxY, length int
	for y, row := range m {
		if y < minY {
			minY = y
		}
		if y > maxY {
			maxY = y
		}
		for x, d := range row {
			if x < minX {
				minX = x
			}
			if x > maxX {
				maxX = x
			}
			if len(fmt.Sprint(d)) > length {
				length = len(fmt.Sprint(d))
			}
		}
	}

	for y := minY; y <= maxY; y++ {
		for x := minX; x <= maxX; x++ {
			if d, ok := m[y][x]; ok {
				fmt.Printf(fmt.Sprintf("%%%dd", length+1), d)
			} else {
				fmt.Print(strings.Repeat(" ", length+1))
			}
		}
		fmt.Println()
	}
}

var offsets = map[rune][2]int{
	'E': {1, 0},
	'W': {-1, 0},
	'N': {0, -1},
	'S': {0, 1},
}

func Walk(regex string, m *Map, doors, x, y int) {
	var level, levelStart int
	oX, oY, sum := x, y, doors

	for i, r := range regex {
		switch r {
		case '(':
			if level == 0 {
				levelStart = i
			}
			level++
		case ')':
			level--
			if level == 0 {
				Walk(regex[levelStart+1:i], m, sum, x, y)
			}
		case '|':
			if level == 0 {
				x, y, sum = oX, oY, doors
			}
		case 'N', 'E', 'S', 'W':
			if level == 0 {
				sum++
				x += offsets[r][0]
				y += offsets[r][1]
				m.Set(sum, x, y)
			}
		}
	}
}
