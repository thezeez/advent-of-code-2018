package day20_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day20"
)

func TestDooriestRoom(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"^WNE$", 3},
		{"^ENWWW(NEEE|SSE(EE|N))$", 10},
		{"^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$", 18},
		{"^ESSWWN(E|NNENN(EESS(WNSE|)SSS|WWWSSSSE(SW|NNNE)))$", 23},
		{"^WSSEESWWWNW(S|NENNEEEENN(ESSSSW(NWSW|SSEN)|WSWWN(E|WWS(E|SS))))$", 31},
	}

	for _, test := range tests {
		actual := day20.DooriestRoom(test.in)
		if actual != test.out {
			t.Errorf("DooriestRoom(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestNumberOfDoors(t *testing.T) {
	tests := []struct {
		in  string
		min int
		out int
	}{
		{"^WNE$", 2, 2},
		{"^ENWWW(NEEE|SSE(EE|N))$", 5, 11},
		{"^ENWWW(NEEE|SSE(EE|N))$", 6, 10},
		{"^ENNWSWW(NEWS|)SSSEEN(WNSE|)EE(SWEN|)NNN$", 9, 15},
	}

	for _, test := range tests {
		actual := day20.NumberOfDoors(test.in, test.min)
		if actual != test.out {
			t.Errorf("NumberOfDoors(%q, %d) => %d, want %d", test.in, test.min, actual, test.out)
		}
	}
}
