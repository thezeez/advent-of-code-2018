package day17_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day17"
)

func TestMap_Flood(t *testing.T) {
	tests := []struct {
		in      []string
		all     int
		settled int
	}{
		{
			[]string{
				"x=495, y=2..7   ",
				"y=7, x=495..501 ",
				"x=501, y=3..7   ",
				"x=498, y=2..4   ",
				"x=506, y=1..2   ",
				"x=498, y=10..13 ",
				"x=504, y=10..13 ",
				"y=13, x=498..504",
			},
			57,
			29,
		},
	}

	for i, test := range tests {
		m := day17.NewMap(test.in)
		m.Flood()
		actualAll := m.Count(day17.KindFlow | day17.KindWater)
		actualSettled := m.Count(day17.KindWater)
		if actualAll != test.all || actualSettled != test.settled {
			t.Errorf("Map.Flood(#%d) => %d,%d, want %d,%d", i, actualAll, actualSettled, test.all, test.settled)
		}
	}
}
