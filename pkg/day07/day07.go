package day07

import (
	"regexp"
	"sort"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

type Step struct {
	Name             string
	Dependencies     []*Step
	Done             bool
	TimeToCompletion int
	Assigned         bool
}

var pattern = regexp.MustCompile(`Step ([A-Z]) must be finished before step ([A-Z]) can begin.`)

func Steps(instructions []string, baseTime int) map[string]*Step {
	steps := make(map[string]*Step, len(instructions))
	for _, step := range instructions {
		match := pattern.FindAllStringSubmatch(step, 1)
		if len(match) == 0 {
			continue
		}
		p := match[0][1]
		s := match[0][2]
		if _, ok := steps[p]; !ok {
			steps[p] = &Step{Name: p, TimeToCompletion: baseTime + 1 + int(p[0]-'A')}
		}
		if _, ok := steps[s]; !ok {
			steps[s] = &Step{Name: s, TimeToCompletion: baseTime + 1 + int(s[0]-'A')}
		}
		if !containsStep(steps[s].Dependencies, steps[p]) {
			steps[s].Dependencies = append(steps[s].Dependencies, steps[p])
		}
	}
	return steps
}

func Order(steps map[string]*Step) string {
	var done string

	for len(steps) > 0 {
		var available []string

		for name, step := range steps {
			if !helpers.ContainsString(available, name) && checkReady(step) {
				available = append(available, name)
			}
		}

		sort.Strings(available)
		done += available[0]
		steps[available[0]].Done = true
		delete(steps, available[0])
	}

	return done
}

func TimeToCompletion(steps map[string]*Step, availableWorkers int) int {
	var timeSpent int

	workers := make([]worker, availableWorkers)

	for len(steps) > 0 {
		timeSpent++
		var available []string

		for name, step := range steps {
			if !helpers.ContainsString(available, name) && checkReady(step) && !step.Done && !step.Assigned {
				available = append(available, name)
			}
		}
		sort.Strings(available)

		for i := range workers {
			if workers[i].Work == nil {
				if len(available) == 0 {
					continue
				}
				workers[i].Work = steps[available[0]]
				workers[i].Work.Assigned = true
				available = available[1:]
			}
			completed := workers[i].work()
			if completed != "" {
				delete(steps, completed)
			}
		}
	}

	return timeSpent
}

func checkReady(step *Step) bool {
	if step.Done {
		return true
	}
	for _, dep := range step.Dependencies {
		if !dep.Done {
			return false
		}
	}
	return true
}

func containsStep(steps []*Step, step *Step) bool {
	for _, ptr := range steps {
		if ptr == step {
			return true
		}
	}
	return false
}

type worker struct {
	Work *Step
}

func (w *worker) work() (out string) {
	if w.Work == nil {
		return
	}
	w.Work.TimeToCompletion--
	if w.Work.TimeToCompletion <= 0 {
		out = w.Work.Name
		w.Work.Done = true
		w.Work.Assigned = false
		w.Work = nil
	}
	return
}
