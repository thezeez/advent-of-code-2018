package day07_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day07"
)

func TestOrder(t *testing.T) {
	tests := []struct {
		in  []string
		out string
	}{
		{[]string{
			"Step C must be finished before step A can begin.",
			"Step C must be finished before step F can begin.",
			"Step A must be finished before step B can begin.",
			"Step A must be finished before step D can begin.",
			"Step B must be finished before step E can begin.",
			"Step D must be finished before step E can begin.",
			"Step F must be finished before step E can begin.",
		}, "CABDFE"},
	}

	for i, test := range tests {
		actual := day07.Order(day07.Steps(test.in, 0))
		if actual != test.out {
			t.Errorf("Order(#%d) => %q, want %q", i, actual, test.out)
		}
	}
}

func TestTimeToCompletion(t *testing.T) {
	tests := []struct {
		in      []string
		time int
		workers int
		out     int
	}{
		{[]string{
			"Step C must be finished before step A can begin.",
			"Step C must be finished before step F can begin.",
			"Step A must be finished before step B can begin.",
			"Step A must be finished before step D can begin.",
			"Step B must be finished before step E can begin.",
			"Step D must be finished before step E can begin.",
			"Step F must be finished before step E can begin.",
		}, 0, 2, 15},
	}

	for i, test := range tests {
		actual := day07.TimeToCompletion(day07.Steps(test.in, test.time), test.workers)
		if actual != test.out {
			t.Errorf("TimeToCompletion(%d) => %d, want %d", i, actual, test.out)
		}
	}
}
