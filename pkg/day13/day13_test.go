package day13_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day13"
)

func TestMoveCarts(t *testing.T) {
	tests := []struct {
		in         []string
		firstCrash bool
		out        day13.Coordinate
	}{
		{[]string{
			`/->-\        `,
			`|   |  /----\`,
			`| /-+--+-\  |`,
			`| | |  | v  |`,
			`\-+-/  \-+--/`,
			`\------/     `,
		}, true, day13.Coordinate{X: 7, Y: 3}},
		{[]string{
			`/>-<\  `,
			`|   |  `,
			`| /<+-\`,
			`| | | v`,
			`\>+</ |`,
			`  |   ^`,
			`  \<->/`,
		}, false, day13.Coordinate{X: 6, Y: 4}},
	}

	for i, test := range tests {
		actual := day13.MoveCarts(test.in, test.firstCrash)
		if actual != test.out {
			t.Errorf("MoveCarts(#%d, %t) => %s, want %s", i, test.firstCrash, actual, test.out)
		}
	}
}
