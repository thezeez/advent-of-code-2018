package day13

import "fmt"

type Coordinate struct {
	X int
	Y int
}

func (c Coordinate) String() string {
	return fmt.Sprintf("%d,%d", c.X, c.Y)
}

type Cart struct {
	Direction        int
	NextIntersection int
	Moved            bool
}

func (c Cart) String() string {
	return directionStrings[c.Direction]
}

var directionStrings = []string{"^", ">", "v", "<"}

var directions = []Coordinate{
	{0, -1},
	{1, 0},
	{0, 1},
	{-1, 0},
}

func MoveCarts(input []string, firstCrash bool) Coordinate {
	carts := make(map[Coordinate]*Cart)
	tracks := make(map[Coordinate]byte)
	var positions []Coordinate

	for y := 0; y < len(input); y++ {
		for x := 0; x < len(input[y]); x++ {
			switch segment := input[y][x]; segment {
			case '^':
				carts[Coordinate{x, y}] = &Cart{Direction: 0, NextIntersection: 0}
				tracks[Coordinate{x, y}] = '|'
				positions = append(positions, Coordinate{x, y})
			case '>':
				carts[Coordinate{x, y}] = &Cart{Direction: 1, NextIntersection: 0}
				tracks[Coordinate{x, y}] = '-'
				positions = append(positions, Coordinate{x, y})
			case 'v':
				carts[Coordinate{x, y}] = &Cart{Direction: 2, NextIntersection: 0}
				tracks[Coordinate{x, y}] = '|'
				positions = append(positions, Coordinate{x, y})
			case '<':
				carts[Coordinate{x, y}] = &Cart{Direction: 3, NextIntersection: 0}
				tracks[Coordinate{x, y}] = '-'
				positions = append(positions, Coordinate{x, y})
			case '|', '/', '-', '\\', '+':
				tracks[Coordinate{x, y}] = input[y][x]
				positions = append(positions, Coordinate{x, y})
			}
		}
	}

	for {
		if !firstCrash {
			if len(carts) <= 1 {
				for pos := range carts {
					return pos
				}
				return Coordinate{-1, -1}
			}
		}

		for _, cart := range carts {
			cart.Moved = false
		}

		for _, pos := range positions {
			if cart, ok := carts[pos]; ok {
				if cart.Moved {
					continue
				}

				newPos := Coordinate{pos.X + directions[cart.Direction].X, pos.Y + directions[cart.Direction].Y}

				if _, ok := carts[newPos]; ok {
					if firstCrash {
						return newPos
					} else {
						delete(carts, pos)
						delete(carts, newPos)
						continue
					}
				}

				switch tracks[newPos] {
				case '/':
					switch cart.Direction {
					case 0:
						cart.Direction = 1
					case 1:
						cart.Direction = 0
					case 2:
						cart.Direction = 3
					case 3:
						cart.Direction = 2
					}
				case '\\':
					switch cart.Direction {
					case 0:
						cart.Direction = 3
					case 1:
						cart.Direction = 2
					case 2:
						cart.Direction = 1
					case 3:
						cart.Direction = 0
					}
				case '+':
					cart.Direction = (((cart.Direction + cart.NextIntersection - 1) % 4) + 4) % 4
					cart.NextIntersection = (cart.NextIntersection + 1) % 3
				}
				cart.Moved = true
				carts[newPos] = cart
				delete(carts, pos)
			}
		}
	}
}
