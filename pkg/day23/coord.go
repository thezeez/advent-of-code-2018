package day23

import helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"

type Coordinate struct {
	X, Y, Z int
}

var Zero = Coordinate{X: 0, Y: 0, Z: 0}

func (c Coordinate) Distance(a Coordinate) int {
	return helpers2017.Abs(c.X-a.X) + helpers2017.Abs(c.Y-a.Y) + helpers2017.Abs(c.Z-a.Z)
}
