package day09_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day09"
)

func TestParseInput(t *testing.T) {
	tests := []struct {
		in      string
		players int
		marbles int
	}{
		{"400 players; last marble is worth 71864 points", 400, 71864},
		{"9 players; last marble is worth 25 points", 9, 25},
	}

	for _, test := range tests {
		players, marbles := day09.ParseInput(test.in)
		if players != test.players || marbles != test.marbles {
			t.Errorf("ParseInput(%q) => %d, %d, want %d, %d", test.in, players, marbles, test.players, test.marbles)
		}
	}
}

func TestMarbles(t *testing.T) {
	tests := []struct {
		players int
		last    int
		out     int
	}{
		{9, 25, 32},
		{10, 1618, 8317},
		{13, 7999, 146373},
		{17, 1104, 2764},
		{21, 6111, 54718},
		{30, 5807, 37305},
	}

	for _, test := range tests {
		actual := day09.Marbles(test.players, test.last)
		if actual != test.out {
			t.Errorf("Marbles(%d, %d) => %d, want %d", test.players, test.last, actual, test.out)
		}
	}
}
