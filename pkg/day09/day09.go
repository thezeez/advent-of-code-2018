package day09

import (
	"container/ring"
	"regexp"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func ParseInput(input string) (int, int) {
	match := regexp.MustCompile(`(\d+) players; last marble is worth (\d+) points`).FindAllStringSubmatch(input, 1)
	return helpers2017.IntOrPanic(match[0][1]), helpers2017.IntOrPanic(match[0][2])
}

func Marbles(players, last int) int {
	circle := ring.New(1)
	circle.Value = 0

	scores := make([]int, players)

	for i := 1; i <= last; i++ {
		if i%23 == 0 {
			circle = circle.Move(-8)

			removed := circle.Unlink(1)
			scores[i%players] += i + removed.Value.(int)

			circle = circle.Move(1)
		} else {
			circle = circle.Move(1)

			s := ring.New(1)
			s.Value = i

			circle.Link(s)
			circle = circle.Move(1)
		}
	}
	var highestScore int
	for _, score := range scores {
		if score > highestScore {
			highestScore = score
		}
	}
	return highestScore
}
