package day12_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day12"
)

func TestPlantGrowth(t *testing.T) {
	tests := []struct {
		state      string
		growth     []string
		iterations int
		out        int
	}{
		{
			"initial state: #..#.#..##......###...###",
			[]string{
				"...## => #",
				"..#.. => #",
				".#... => #",
				".#.#. => #",
				".#.## => #",
				".##.. => #",
				".#### => #",
				"#.#.# => #",
				"#.### => #",
				"##.#. => #",
				"##.## => #",
				"###.. => #",
				"###.# => #",
				"####. => #",
			},
			20,
			325,
		},
	}

	for i, test := range tests {
		actual := day12.PlantGrowth(test.state, test.growth, test.iterations)
		if actual != test.out {
			t.Errorf("PlantGrowth(#%d, %d) => %d, want %d", i, test.iterations, actual, test.out)
		}
	}
}
