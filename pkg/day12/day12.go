package day12

import (
	"math"
)

const minStableGenerations = 10

type Change struct {
	State  map[int]bool
	Growth bool
}

func PlantGrowth(initial string, growth []string, iterations int) int {
	pots := make(map[int]bool)
	initial = initial[len("initial state: "):]

	for i := 0; i < len(initial); i++ {
		pots[i] = initial[i] == '#'
	}

	smallest := 0
	largest := len(initial)

	var changes []Change
	for _, change := range growth {
		if change == "" {
			continue
		}
		states := make(map[int]bool, 5)
		for i := 0; i < 5; i++ {
			states[i-2] = change[i] == '#'
		}
		changes = append(changes, Change{State: states, Growth: change[9] == '#'})
	}

	var previousSum, previousDelta, stableFor int

	for i := 1; i <= iterations; i++ {
		newSmallest := math.MaxInt64
		newLargest := math.MinInt64
		nextGen := make(map[int]bool)
		for potIndex := smallest - 2; potIndex < largest+2; potIndex++ {
		changeSearch:
			for _, change := range changes {
				for offset, expectedState := range change.State {
					if pots[potIndex+offset] != expectedState {
						continue changeSearch
					}
				}
				nextGen[potIndex] = change.Growth
				break changeSearch
			}
			if nextGen[potIndex] {
				if potIndex < newSmallest {
					newSmallest = potIndex
				}
				if potIndex > newLargest {
					newLargest = potIndex
				}
			}
		}
		pots = nextGen
		smallest = newSmallest
		largest = newLargest

		sum := sumPots(pots)
		if sum-previousSum == previousDelta {
			stableFor++
		} else {
			stableFor = 0
		}
		if stableFor > minStableGenerations {
			return sum + ((iterations - i) * previousDelta)
		}
		previousDelta = sum - previousSum
		previousSum = sum
	}
	
	return sumPots(pots)
}

func sumPots(pots map[int]bool) int {
	var sum int
	for pos, plant := range pots {
		if plant {
			sum += pos
		}
	}
	return sum
}
