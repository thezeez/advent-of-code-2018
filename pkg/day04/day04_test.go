package day04_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day04"
)

var expectedSchedule = map[int]*day04.Guard{
	10: {
		Id:            10,
		MinutesAsleep: 50,
		SleepSchedule: map[int]int{
			5:  1,
			6:  1,
			7:  1,
			8:  1,
			9:  1,
			10: 1,
			11: 1,
			12: 1,
			13: 1,
			14: 1,
			15: 1,
			16: 1,
			17: 1,
			18: 1,
			19: 1,
			20: 1,
			21: 1,
			22: 1,
			23: 1,
			24: 2,
			25: 1,
			26: 1,
			27: 1,
			28: 1,
			30: 1,
			31: 1,
			32: 1,
			33: 1,
			34: 1,
			35: 1,
			36: 1,
			37: 1,
			38: 1,
			39: 1,
			40: 1,
			41: 1,
			42: 1,
			43: 1,
			44: 1,
			45: 1,
			46: 1,
			47: 1,
			48: 1,
			49: 1,
			50: 1,
			51: 1,
			52: 1,
			53: 1,
			54: 1,
		},
	},
	99: {
		Id:            99,
		MinutesAsleep: 30,
		SleepSchedule: map[int]int{
			36: 1,
			37: 1,
			38: 1,
			39: 1,
			40: 2,
			41: 2,
			42: 2,
			43: 2,
			44: 2,
			45: 3,
			46: 2,
			47: 2,
			48: 2,
			49: 2,
			50: 1,
			51: 1,
			52: 1,
			53: 1,
			54: 1,
		},
	},
}

func TestSleepSchedule(t *testing.T) {
	test := struct {
		in []string
	}{
		[]string{
			"[1518-11-01 00:00] Guard #10 begins shift",
			"[1518-11-01 00:05] falls asleep",
			"[1518-11-01 00:25] wakes up",
			"[1518-11-01 00:30] falls asleep",
			"[1518-11-01 00:55] wakes up",
			"[1518-11-01 23:58] Guard #99 begins shift",
			"[1518-11-02 00:40] falls asleep",
			"[1518-11-02 00:50] wakes up",
			"[1518-11-03 00:05] Guard #10 begins shift",
			"[1518-11-03 00:24] falls asleep",
			"[1518-11-03 00:29] wakes up",
			"[1518-11-04 00:02] Guard #99 begins shift",
			"[1518-11-04 00:36] falls asleep",
			"[1518-11-04 00:46] wakes up",
			"[1518-11-05 00:03] Guard #99 begins shift",
			"[1518-11-05 00:45] falls asleep",
			"[1518-11-05 00:55] wakes up",
		},
	}

	actual := day04.SleepSchedule(test.in)
	for id, guard := range expectedSchedule {
		actualGuard, ok := actual[id]
		if !ok {
			t.Errorf("SleepSchedule missing %d", id)
		} else {
			if guard.MinutesAsleep != actualGuard.MinutesAsleep {
				t.Errorf("SleepSchedule total minutes differs for guard %d => %d, want %d", id, actualGuard.MinutesAsleep, guard.MinutesAsleep)
			}
			for i := 0; i < 60; i++ {
				if guard.SleepSchedule[i] != actualGuard.SleepSchedule[i] {
					t.Errorf("SleepSchedule differs for guard %d at %d => %d, want %d", id, i, actualGuard.SleepSchedule[i], guard.SleepSchedule[i])
				}
			}
		}
	}
}

func TestMostAsleep(t *testing.T) {
	tests := []struct {
		in  map[int]*day04.Guard
		out int
	}{
		{expectedSchedule, 240},
	}

	for _, test := range tests {
		actual := day04.MostAsleep(test.in)
		if actual != test.out {
			t.Errorf("MostAsleep(%v) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestMostCommonMinute(t *testing.T) {
	tests := []struct {
		in  map[int]*day04.Guard
		out int
	}{
		{expectedSchedule, 4455},
	}

	for _, test := range tests {
		actual := day04.MostCommonMinute(test.in)
		if actual != test.out {
			t.Errorf("MostCommonMinute(%v) => %d, want %d", test.in, actual, test.out)
		}
	}
}
