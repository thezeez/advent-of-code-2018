package day04

import (
	"regexp"
	"sort"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

type Guard struct {
	Id            int
	MinutesAsleep int
	SleepSchedule map[int]int
}

var timestampPattern = regexp.MustCompile(`^\[\d+-\d+-\d+ \d+:(\d+)] (?:(?:Guard #(\d+) begins shift)|(falls asleep)|(wakes up))$`)

const (
	matchMinute = iota + 1
	matchGuardId
	matchFallAsleep
	matchWakeUp
)

func SleepSchedule(cycle []string) map[int]*Guard {
	var activeGuard, asleepSince int
	guards := make(map[int]*Guard)

	sort.Strings(cycle)

	for _, event := range cycle {
		match := timestampPattern.FindAllStringSubmatch(event, -1)
		if match == nil {
			continue
		}

		switch sub := match[0]; true {
		case sub[matchGuardId] != "":
			id := helpers2017.IntOrPanic(sub[matchGuardId])
			activeGuard = id
			if _, ok := guards[id]; !ok {
				guards[id] = &Guard{id, 0, map[int]int{}}
			}

		case sub[matchFallAsleep] != "":
			asleepSince = helpers2017.IntOrPanic(sub[matchMinute])

		case sub[matchWakeUp] != "":
			awakeSince := helpers2017.IntOrPanic(sub[matchMinute])
			guards[activeGuard].MinutesAsleep += awakeSince - asleepSince
			for i := asleepSince; i < awakeSince; i++ {
				guards[activeGuard].SleepSchedule[i]++
			}
		}
	}
	return guards
}

func MostAsleep(guards map[int]*Guard) int {
	mostAsleepGuard := -1
	for id, guard := range guards {
		if mostAsleepGuard < 0 || guard.MinutesAsleep > guards[mostAsleepGuard].MinutesAsleep {
			mostAsleepGuard = id
		}
	}

	commonMinute := -1
	for minute, occurrences := range guards[mostAsleepGuard].SleepSchedule {
		if commonMinute < 0 || guards[mostAsleepGuard].SleepSchedule[commonMinute] < occurrences {
			commonMinute = minute
		}
	}
	return commonMinute * guards[mostAsleepGuard].Id
}

func MostCommonMinute(guards map[int]*Guard) int {
	var mostCommonMinute, mostCommonGuard, mostCommonOccurrences int

	for i := 0; i < 60; i++ {
		for id, guard := range guards {
			if guard.SleepSchedule[i] > mostCommonOccurrences {
				mostCommonOccurrences = guard.SleepSchedule[i]
				mostCommonGuard = id
				mostCommonMinute = i
			}
		}
	}
	return mostCommonGuard * mostCommonMinute
}
