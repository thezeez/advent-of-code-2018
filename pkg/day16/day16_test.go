package day16_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day16"
)

func TestTripleSamples(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{
			"Before: [3, 2, 1, 1]",
			"9 2 1 2",
			"After:  [3, 2, 2, 1]",
		}, 1},
	}

	for _, test := range tests {
		actual := day16.TripleSamples(test.in)
		if actual != test.out {
			t.Errorf("TripleSamples(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
