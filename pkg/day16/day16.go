package day16

import "fmt"

type Machine struct {
	Registers [4]int
}

func (m *Machine) Do(opcode, a, b, c int) {
	OpcodeInstructions[opcode](m, a, b, c)
}

//noinspection SpellCheckingInspection
const (
	OpcodeAddr = iota
	OpcodeAddi
	OpcodeMulr
	OpcodeMuli
	OpcodeBanr
	OpcodeBani
	OpcodeBorr
	OpcodeBori
	OpcodeSetr
	OpcodeSeti
	OpcodeGtir
	OpcodeGtri
	OpcodeGtrr
	OpcodeEqir
	OpcodeEqri
	OpcodeEqrr
	NumOpcodes
)

var OpcodeInstructions = map[int]Instruction{
	OpcodeAddr: (*Machine).Addr,
	OpcodeAddi: (*Machine).Addi,
	OpcodeMulr: (*Machine).Mulr,
	OpcodeMuli: (*Machine).Muli,
	OpcodeBanr: (*Machine).Banr,
	OpcodeBani: (*Machine).Bani,
	OpcodeBorr: (*Machine).Borr,
	OpcodeBori: (*Machine).Bori,
	OpcodeSetr: (*Machine).Setr,
	OpcodeSeti: (*Machine).Seti,
	OpcodeGtir: (*Machine).Gtir,
	OpcodeGtri: (*Machine).Gtri,
	OpcodeGtrr: (*Machine).Gtrr,
	OpcodeEqir: (*Machine).Eqir,
	OpcodeEqri: (*Machine).Eqri,
	OpcodeEqrr: (*Machine).Eqrr,
}

//noinspection GoUnusedGlobalVariable,SpellCheckingInspection
var OpcodeNames = map[int]string{
	OpcodeAddr: "Addr",
	OpcodeAddi: "Addi",
	OpcodeMulr: "Mulr",
	OpcodeMuli: "Muli",
	OpcodeBanr: "Banr",
	OpcodeBani: "Bani",
	OpcodeBorr: "Borr",
	OpcodeBori: "Bori",
	OpcodeSetr: "Setr",
	OpcodeSeti: "Seti",
	OpcodeGtir: "Gtir",
	OpcodeGtri: "Gtri",
	OpcodeGtrr: "Gtrr",
	OpcodeEqir: "Eqir",
	OpcodeEqri: "Eqri",
	OpcodeEqrr: "Eqrr",
}

func TripleSamples(input []string) int {
	var numSamples int

	for i := 0; i < len(input); i += 3 {
		before, op, after := input[i], input[i+1], input[i+2]
		if before[0] != 'B' {
			break
		}

		var r1, r2, r3, r4, opcode, a, b, c, found int
		o := Machine{}

		_, err := fmt.Sscanf(before, "Before: [%d,%d,%d,%d]", &r1, &r2, &r3, &r4)
		if err != nil {
			panic(err)
		}
		_, err = fmt.Sscanf(after, "After: [%d,%d,%d,%d]", &o.Registers[0], &o.Registers[1], &o.Registers[2], &o.Registers[3])
		if err != nil {
			panic(err)
		}
		_, err = fmt.Sscanf(op, "%d %d %d %d", &opcode, &a, &b, &c)
		if err != nil {
			panic(err)
		}

		for code := 0; code < NumOpcodes; code++ {
			m := Machine{Registers: [4]int{r1, r2, r3, r4}}

			m.Do(code, a, b, c)
			if m == o {
				found++
			}
		}

		if found >= 3 {
			numSamples++
		}
	}

	return numSamples
}

func ExecuteProgram(input []string) int {
	actualOpcodes := FigureOutOpcodes(input)

	var i int
	for i = 0; i < len(input); i += 3 {
		before := input[i]
		if before[0] != 'B' {
			break
		}
	}

	m := Machine{}

	for ; i < len(input); i++ {
		var opcode, a, b, c int
		parsed, err := fmt.Sscanf(input[i], "%d %d %d %d", &opcode, &a, &b, &c)
		if err != nil || parsed != 4 {
			continue
		}
		opcode = actualOpcodes[opcode]

		m.Do(opcode, a, b, c)
	}

	return m.Registers[0]
}

func FigureOutOpcodes(input []string) map[int]int {
	candidates := make(map[int]map[int]bool)

	for i := 0; i < len(input); i += 3 {
		before, op, after := input[i], input[i+1], input[i+2]
		if before[0] != 'B' {
			break
		}

		var r1, r2, r3, r4, opcode, a, b, c int
		potential := make(map[int]bool)
		o := Machine{}

		_, err := fmt.Sscanf(before, "Before: [%d,%d,%d,%d]", &r1, &r2, &r3, &r4)
		if err != nil {
			panic(err)
		}
		_, err = fmt.Sscanf(after, "After: [%d,%d,%d,%d]", &o.Registers[0], &o.Registers[1], &o.Registers[2], &o.Registers[3])
		if err != nil {
			panic(err)
		}
		_, err = fmt.Sscanf(op, "%d %d %d %d", &opcode, &a, &b, &c)
		if err != nil {
			panic(err)
		}

		for code := 0; code < NumOpcodes; code++ {
			m := Machine{Registers: [4]int{r1, r2, r3, r4}}

			m.Do(code, a, b, c)
			if m == o {
				potential[code] = true
			}
		}

		if _, ok := candidates[opcode]; !ok {
			candidates[opcode] = potential
		} else {
			for pc := range candidates[opcode] {
				if _, ok := potential[pc]; !ok {
					delete(candidates[opcode], pc)
				}
			}
		}
	}

	actualOpcodes := make(map[int]int)
	for len(candidates) > 0 {
		hadMatch := false

		for givenOpcode, potentialOpcodes := range candidates {
			switch len(potentialOpcodes) {
			case 0:
				panic(fmt.Sprintf("No more candidates for opcode %d", givenOpcode))
			case 1:
				hadMatch = true
				for k := range potentialOpcodes {
					actualOpcodes[givenOpcode] = k
				}
				delete(candidates, givenOpcode)
				for k := range candidates {
					delete(candidates[k], actualOpcodes[givenOpcode])
				}
			}
		}

		if !hadMatch {
			panic(fmt.Sprintf("Could not figure out remaining %d opcodes", len(candidates)))
		}
	}

	return actualOpcodes
}
