package day02

import (
	"strings"
)

func Checksum(ids []string) int {
	var result2, result3 int
	for _, id := range ids {
		two, three := Count(id)
		result2 += two
		result3 += three
	}
	return result2 * result3
}

func Common(ids []string) string {
	for i, a := range ids {
		for j, b := range ids {
			if i == j {
				continue
			}
			var found bool
			var diff int
			for k := 0; k < len(a) && k < len(b); k++ {
				if a[k] != b[k] {
					if found {
						found = false
						break
					}
					found = true
					diff = k
				}
			}
			if found {
				return a[:diff] + a[diff+1:]
			}
		}
	}
	return ""
}

func Count(id string) (two, three int) {
	for _, r := range id {
		switch strings.Count(id, string(r)) {
		case 2:
			two = 1
		case 3:
			three = 1
		}
	}
	return
}
