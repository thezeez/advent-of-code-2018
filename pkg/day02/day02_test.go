package day02_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day02"
)

func TestCount(t *testing.T) {
	tests := []struct {
		in    string
		two   int
		three int
	}{
		{"abcdef", 0, 0},
		{"bababc", 1, 1},
		{"abbcde", 1, 0},
		{"abcccd", 0, 1},
		{"aabcdd", 1, 0},
		{"abcdee", 1, 0},
		{"ababab", 0, 1},
	}

	for _, test := range tests {
		actual2, actual3 := day02.Count(test.in)
		if actual2 != test.two || actual3 != test.three {
			t.Errorf("Count(%q) => (%d, %d), want (%d, %d)", test.in, actual2, actual3, test.two, test.three)
		}
	}
}

func TestChecksum(t *testing.T) {
	tests := []struct {
		in  []string
		out int
	}{
		{[]string{"abcdef", "bababc", "abbcde", "abcccd", "aabcdd", "abcdee", "ababab"}, 12},
	}

	for _, test := range tests {
		actual := day02.Checksum(test.in)
		if actual != test.out {
			t.Errorf("Checksum(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestCommon(t *testing.T) {
	tests := []struct {
		in  []string
		out string
	}{
		{[]string{"abcde", "fghij", "klmno", "pqrst", "fguij", "axcye", "wvxyz"}, "fgij"},
	}

	for _, test := range tests {
		actual := day02.Common(test.in)
		if actual != test.out {
			t.Errorf("Common(%q) => %q, want %q", test.in, actual, test.out)
		}
	}
}
