package helpers

func ContainsString(l []string, t string) bool {
	for _, s := range l {
		if s == t {
			return true
		}
	}
	return false
}
