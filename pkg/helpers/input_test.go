package helpers_test

import (
	"reflect"
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func TestToSliceOfInt(t *testing.T) {
	tests := []struct {
		in  string
		sep string
		out []int
	}{
		{"+1, 0, -1", ",", []int{1, 0, -1}},
		{"020 \n -0 \n -020", "\n", []int{20, 0, -20}},
		{"333 222 111 0 -111 -222 -333", " ", []int{333, 222, 111, 0, -111, -222, -333}},
	}

	for _, test := range tests {
		actual := helpers.ToSliceOfInt(test.in, test.sep)
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("ToSliceOfInt(%q, %q) => %+v, want %+v", test.in, test.sep, actual, test.out)
		}
	}
}

func TestCleanSliceOfStrings(t *testing.T) {
	tests := []struct {
		in  string
		sep string
		out []string
	}{
		{"a, b, cd, efgh", ",", []string{"a", "b", "cd", "efgh"}},
		{"a b c \n def\n \t\rghi\t \t \r \t\n\n   \t\r\njklmn", "\n", []string{"a b c", "def", "ghi", "jklmn"}},
	}

	for _, test := range tests {
		actual := helpers.CleanSliceOfStrings(test.in, test.sep)
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("CleanSliceOfStrings(%q, %q) => %q, want %q", test.in, test.sep, actual, test.out)
		}
	}
}
