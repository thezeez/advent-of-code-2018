package helpers

import (
	"strings"

	"bitbucket.org/thezeez/advent-of-code-2017/helpers"
)

func ToSliceOfInt(input, sep string) []int {
	var result []int
	for _, line := range strings.Split(input, sep) {
		if strings.TrimSpace(line) == "" {
			continue
		}
		result = append(result, helpers.IntOrPanic(line))
	}
	return result
}

func CleanSliceOfStrings(input, sep string) []string {
	var out []string
	for _, line := range strings.Split(input, sep) {
		t := strings.TrimSpace(line)
		if t != "" {
			out = append(out, t)
		}
	}
	return out
}
