package helpers_test

import (
	"reflect"
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func TestContainsString(t *testing.T) {
	tests := []struct {
		in    []string
		check string
		out   bool
	}{
		{[]string{"A", "B", "C"}, "B", true},
		{[]string{"A", "B", "C"}, "D", false},
		{[]string{}, "A", false},
	}

	for i, test := range tests {
		actual := helpers.ContainsString(test.in, test.check)
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("ContainsString(#%d, %q) => %t, want %t", i, test.check, actual, test.out)
		}
	}
}
