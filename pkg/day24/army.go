package day24

type Army []*Group

const (
	ArmyImmuneSystem = iota + 1
	ArmyInfection
	ArmyCount
)

var StringArmies = map[string]int{
	"Immune System": ArmyImmuneSystem,
	"Infection":     ArmyInfection,
}

func (a Army) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

func (a Army) Len() int {
	return len(a)
}

func (a Army) Less(i, j int) bool {
	if a[i].EffectivePower() > a[j].EffectivePower() {
		return true
	}
	return a[i].EffectivePower() == a[j].EffectivePower() && a[i].Initiative > a[j].Initiative
}

func (a Army) Alive() bool {
	for _, g := range a {
		if g.Units > 0 {
			return true
		}
	}
	return false
}

func (a Army) Boost(amount int) {
	for _, g := range a {
		g.AttackDamage += amount
	}
}
