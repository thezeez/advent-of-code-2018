package day24

import (
	"fmt"
	"strings"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

type Group struct {
	Units        int
	HitPoints    int
	AttackDamage int
	AttackType   string
	Initiative   int
	Immunities   []string
	Weaknesses   []string

	Attacker *Group
	Target   *Group
}

func (g Group) EffectivePower() int {
	return g.Units * g.AttackDamage
}

func (g Group) DamageDealt(e *Group) int {
	if helpers.ContainsString(e.Immunities, g.AttackType) {
		return 0
	}
	if helpers.ContainsString(e.Weaknesses, g.AttackType) {
		return g.EffectivePower() * 2
	}
	return g.EffectivePower()
}

func (g Group) String() string {
	out := fmt.Sprintf("%d units each with %d hit points", g.Units, g.HitPoints)
	if len(g.Immunities) > 0 || len(g.Weaknesses) > 0 {
		out += " ("
		if len(g.Immunities) > 0 {
			out += "immune to " + strings.Join(g.Immunities, " and ")
			if len(g.Weaknesses) > 0 {
				out += "; "
			}
		}
		if len(g.Weaknesses) > 0 {
			out += "weak to " + strings.Join(g.Weaknesses, " and ")
		}
		out += ")"
	}
	out += fmt.Sprintf(" with an attack that does %d %s damage at initiative %d", g.AttackDamage, g.AttackType, g.Initiative)
	return out
}
