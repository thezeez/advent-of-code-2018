package day24

func ConditionFight(input []string) int {
	battle, initiative := PrepareForBattle(input)

	for battle.Active() {
		battle.FindTargets()
		initiative.Attack()

		battle.Clean()
		initiative.Clean()
	}

	_, units := battle.Result()
	return units
}

func ImmuneSystemBoost(input []string) int {
	var boost int

	for {
		var stalemate bool
		battle, initiative := PrepareForBattle(input)

		battle[ArmyImmuneSystem].Boost(boost)

		for battle.Active() {
			before := battle.TotalUnits()

			battle.FindTargets()
			initiative.Attack()

			if battle.TotalUnits() == before {
				stalemate = true
				break
			}
			battle.Clean()
			initiative.Clean()
		}

		if !stalemate {
			winner, units := battle.Result()
			if winner == ArmyImmuneSystem {
				return units
			}
		}

		boost++
	}
}
