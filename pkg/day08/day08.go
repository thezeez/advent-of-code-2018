package day08

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

type Node struct {
	Children []*Node
	Metadata []int
}

func NewNode(children, metadata int, data []int) (*Node, []int) {
	var child *Node
	var node Node

	for i := 0; i < children; i++ {
		child, data = NewNode(data[0], data[1], data[2:])
		node.Children = append(node.Children, child)
	}
	node.Metadata = data[:metadata]
	return &node, data[metadata:]
}

func ParseLicense(input string) *Node {
	var data []int

	entries := helpers.CleanSliceOfStrings(input, " ")

	for _, entry := range entries {
		data = append(data, helpers2017.IntOrPanic(entry))
	}

	node, _ := NewNode(data[0], data[1], data[2:])
	return node
}

func SumMetadata(node *Node) int {
	var sum int

	for i := range node.Children {
		sum += SumMetadata(node.Children[i])
	}
	for i := range node.Metadata {
		sum += node.Metadata[i]
	}
	return sum
}

func NodeValue(node *Node) int {
	var value int

	if len(node.Children) == 0 {
		value = SumMetadata(node)
	} else {
		for i := range node.Metadata {
			if node.Metadata[i] > 0 && node.Metadata[i] <= len(node.Children) {
				value += NodeValue(node.Children[node.Metadata[i]-1])
			}
		}
	}
	return value
}

func (node *Node) String() string {
	out := "Node{Children:["
	for i, child := range node.Children {
		out += child.String()
		if i < len(node.Children)-1 {
			out += ", "
		}
	}
	out += fmt.Sprintf("] Metadata:%v}", node.Metadata)
	return out
}
