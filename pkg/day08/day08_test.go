package day08_test

import (
	"reflect"
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day08"
)

func TestParseLicense(t *testing.T) {
	tests := []struct {
		in  string
		out *day08.Node
	}{
		{"2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2", &day08.Node{
			Children: []*day08.Node{
				&day08.Node{
					Metadata: []int{10, 11, 12},
				},
				&day08.Node{
					Children: []*day08.Node{
						&day08.Node{
							Metadata: []int{99},
						},
					},
					Metadata: []int{2},
				},
			},
			Metadata: []int{1, 1, 2},
		}},
	}

	for _, test := range tests {
		actual := day08.ParseLicense(test.in)
		if !reflect.DeepEqual(actual, test.out) {
			t.Errorf("ParseLicense(%q) => %+v, want %+v", test.in, actual, test.out)
		}
	}
}

func TestSumMetadata(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2", 138},
	}

	for _, test := range tests {
		actual := day08.SumMetadata(day08.ParseLicense(test.in))
		if actual != test.out {
			t.Errorf("SumMetadata(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}

func TestNodeValue(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"2 3 0 3 10 11 12 1 1 0 1 99 2 1 1 2", 66},
	}

	for _, test := range tests {
		actual := day08.NodeValue(day08.ParseLicense(test.in))
		if actual != test.out {
			t.Errorf("NodeValue(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
