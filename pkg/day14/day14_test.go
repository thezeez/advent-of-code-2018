package day14_test

import (
	"testing"

	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day14"
)

func TestScoresAfter(t *testing.T) {
	tests := []struct {
		in  int
		out int
	}{
		{9, 5158916779},
		{5, 124515891},
		{18, 9251071085},
		{2018, 5941429882},
	}

	for _, test := range tests {
		actual := day14.ScoresAfter(test.in)
		if actual != test.out {
			t.Errorf("ScoresAfter(%d) => %010d, want %010d", test.in, actual, test.out)
		}
	}
}

func TestScoresBefore(t *testing.T) {
	tests := []struct {
		in  string
		out int
	}{
		{"51589", 9},
		{"01245", 5},
		{"12451", 6},
		{"010124", 3},
		{"92510", 18},
		{"59414", 2018},
	}

	for _, test := range tests {
		actual := day14.ScoresBefore(test.in)
		if actual != test.out {
			t.Errorf("ScoresBefore(%q) => %d, want %d", test.in, actual, test.out)
		}
	}
}
