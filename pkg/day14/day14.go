package day14

import (
	"bytes"
	"math"
	"strings"
)

const amount = 10

func ScoresAfter(numRecipes int) int {
	scores := []int{3, 7}
	elf1, elf2 := 0, 1

	for i := 2; i < numRecipes+amount; i++ {
		score := scores[elf1] + scores[elf2]
		if score >= 10 {
			scores = append(scores, 1, score%10)
		} else {
			scores = append(scores, score)
		}
		elf1 = (elf1 + 1 + scores[elf1]) % len(scores)
		elf2 = (elf2 + 1 + scores[elf2]) % len(scores)
	}

	var result int
	for i := 0; i < amount; i++ {
		result += scores[numRecipes+i] * int(math.Pow10(amount-i-1))
	}
	return result
}

func ScoresBefore(targetScore string) int {
	var target []byte
	for _, r := range strings.TrimSpace(targetScore) {
		target = append(target, byte(r-'0'))
	}

	scores := []byte{3, 7}
	elf1, elf2 := 0, 1

	for {
		score := scores[elf1] + scores[elf2]
		if score >= 10 {
			scores = append(scores, 1)
			if len(scores) >= len(target) && bytes.Equal(scores[len(scores)-len(target):], target) {
				return len(scores) - len(target)
			}
			scores = append(scores, score%10)
			if len(scores) >= len(target) && bytes.Equal(scores[len(scores)-len(target):], target) {
				return len(scores) - len(target)
			}
		} else {
			scores = append(scores, score)
			if len(scores) >= len(target) && bytes.Equal(scores[len(scores)-len(target):], target) {
				return len(scores) - len(target)
			}
		}
		elf1 = (elf1 + 1 + int(scores[elf1])) % len(scores)
		elf2 = (elf2 + 1 + int(scores[elf2])) % len(scores)
	}
}
