package day10

import (
	"fmt"
	"math"
)

type Coordinate struct {
	X, Y int
}

type Point struct {
	X, Y, Vx, Vy int
}

type set struct{}

var offsets = []Coordinate{
	{0, -1},
	{0, 1},
	{-1, 0},
	{1, 0},
}

func GetPoints(input []string) []Point {
	points := make([]Point, len(input))
	for i, p := range input {
		var x, y, dx, dy int
		_, err := fmt.Sscanf(p, "position=<%d, %d> velocity=<%d, %d>", &x, &y, &dx, &dy)
		if err != nil {
			panic(err)
		}
		points[i] = Point{x, y, dx, dy}
	}
	return points
}

func Message(points []Point) ([]string, int) {
	var t int
	for {
		t++
		var maxX, maxY, minX, minY int
		minX = math.MaxInt32
		minY = math.MaxInt32

		m := make(map[Coordinate]set, len(points))

		for i, p := range points {
			x, y := p.X+p.Vx, p.Y+p.Vy
			points[i].X = x
			points[i].Y = y
			m[Coordinate{x, y}] = set{}

			if x > maxX {
				maxX = x
			}
			if y > maxY {
				maxY = y
			}
			if x < minX {
				minX = x
			}
			if y < minY {
				minY = y
			}

		}

		var neighbours int
		for _, point := range points {
			for _, offset := range offsets {
				if _, ok := m[Coordinate{point.X + offset.X, point.Y + offset.Y}]; ok {
					neighbours++
				}
			}
		}

		if neighbours > len(points) {
			var out []string
			for y := minY; y <= maxY; y++ {
				var line string
				for x := minX; x <= maxX; x++ {
					if _, ok := m[Coordinate{x, y}]; ok {
						line += "#"
					} else {
						line += "."
					}
				}
				out = append(out, line)
			}
			return out, t
		}
	}
}
