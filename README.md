This repository is a collection of my solutions for the [Advent of Code 2018](http://adventofcode.com/2018) calendar.

Puzzle                                                    | Silver                | Gold
--------------------------------------------------------- | --------------------- | ---------------------
[Day 1: Chronal Calibration](pkg/day01)                   | `00:20:20`   (`1609`) | `00:29:49`   (`1059`)
[Day 2: Inventory Management System](pkg/day02)           | `00:22:32`   (`1799`) | `01:15:51`   (`2548`)
[Day 3: No Matter How You Slice It](pkg/day03)            | `00:30:29`   (`1476`) | `00:39:26`   (`1323`)
[Day 4: Repose Record](pkg/day04)                         | `00:33:33`   (` 534`) | `00:38:48`   (` 501`)
[Day 5: Alchemical Reduction](pkg/day05)                  | `00:26:58`   (`1529`) | `00:38:49`   (`1302`)
[Day 6: Chronal Coordinates](pkg/day06) [^1]              | `01:43:34`   (`1707`) | `01:59:11`   (`1637`)
[Day 7: The Sum of Its Parts](pkg/day07)                  | `01:06:02`   (`1660`) | `01:50:09`   (`1231`)
[Day 8: Memory Maneuver](pkg/day08)                       | `00:22:27`   (` 706`) | `00:35:01`   (` 685`)
[Day 9: Marble Mania](pkg/day09)                          | `01:03:36`   (`1184`) | `01:04:35`   (` 498`)
[Day 10: The Stars Align](pkg/day10)                      | `00:41:12`   (` 761`) | `00:45:05`   (` 819`)
[Day 11: Chronal Charge](pkg/day11)                       | `00:23:33`   (`1010`) | `00:52:17`   (` 966`)
[Day 12: Subterranean Sustainability](pkg/day12)          | `00:34:19`   (` 597`) | `01:12:25`   (` 720`)
[Day 13: Mine Cart Madness](pkg/day13)                    | `01:08:03`   (` 609`) | `01:20:32`   (` 448`)
[Day 14: Chocolate Charts](pkg/day14)                     | `00:45:29`   (`1189`) | `01:40:34`   (`1255`)
[Day 15: Beverage Bandits](pkg/day15)                     | `08:31:56`   (` 799`) | `37:31:38`   (`2339`)
[Day 16: Chronal Classification](pkg/day16)               | `01:11:06`   (` 742`) | `01:43:07`   (` 635`)
[Day 17: Reservoir Research](pkg/day17)                   | `12:16:55`   (`1389`) | `13:27:53`   (`1472`)
[Day 18: Settlers of The North Pole](pkg/day18)           | `00:36:43`   (` 707`) | `00:48:40`   (` 494`)
[Day 19: Go With The Flow](pkg/day19)                     | `01:20:30`   (` 900`) | `01:42:54`   (` 381`)
[Day 20: A Regular Map](pkg/day20)                        | `00:54:38`   (` 109`) | `02:10:36`   (` 379`)
[Day 21: Chronal Conversion](pkg/day21)                   | `01:37:48`   (` 630`) | `02:18:43`   (` 436`)
[Day 22: Mode Maze](pkg/day22)                            | `00:49:58`   (` 557`) | `03:16:21`   (` 439`)
[Day 23: Experimental Emergency Teleportation](pkg/day23) | `00:20:47`   (` 561`) | `02:50:29`   (` 229`)
[Day 24: Immune System Simulator 20XX](pkg/day24)         | `03:15:48`   (` 525`) | `03:53:30`   (` 514`)
[Day 25: Four-Dimensional Adventure](pkg/day25)           | `00:44:42`   (` 524`) | `00:45:25`   (` 376`)

[^1]: Correct answer was not accepted for 90 minutes due to
      [a bug](https://twitter.com/ericwastl/status/1070568609188143104) :-(
