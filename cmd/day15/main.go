package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day15"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	cave := helpers.CleanSliceOfStrings(input, "\n")

	fmt.Printf("Combat: %d\n", day15.Combat(cave))
	fmt.Printf("CheatingElves: %d\n", day15.CheatingElves(cave))
}
