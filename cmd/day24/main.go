package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day24"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	armies := helpers.CleanSliceOfStrings(input, "\n")

	fmt.Printf("ConditionFight: %d\n", day24.ConditionFight(armies))
	fmt.Printf("ImmuneSystemBoost: %d\n", day24.ImmuneSystemBoost(armies))
}
