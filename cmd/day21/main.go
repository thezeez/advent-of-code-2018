package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day21"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	instructions := helpers.CleanSliceOfStrings(input, "\n")

	fmt.Printf("FewestInstructions: %d\n", day21.FewestInstructions(instructions))
	fmt.Printf("MostInstructions: %d\n", day21.MostInstructions(instructions))
}
