package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day22"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	fmt.Printf("RiskLevel: %d\n", day22.RiskLevel(input))
	fmt.Printf("Rescue: %d\n", day22.Rescue(input))
}
