package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day18"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	outskirts := helpers.CleanSliceOfStrings(input, "\n")

	fmt.Printf("LumberCollection (10 minutes): %d\n", day18.LumberCollection(outskirts, 10))
	fmt.Printf("LumberCollection (1901 years): %d\n", day18.LumberCollection(outskirts, 1e9))
}
