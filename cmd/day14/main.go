package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day14"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")
	recipes := helpers2017.IntOrPanic(input)

	fmt.Printf("ScoresAfter: %010d\n", day14.ScoresAfter(recipes))
	fmt.Printf("ScoresBefore: %d\n", day14.ScoresBefore(input))
}
