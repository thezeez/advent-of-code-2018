package main

import (
	"fmt"
	"strings"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day13"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")
	track := strings.Split(input, "\n")

	fmt.Printf("First crash: %s\n", day13.MoveCarts(track, true))
	fmt.Printf("Last cart: %s\n", day13.MoveCarts(track, false))
}
