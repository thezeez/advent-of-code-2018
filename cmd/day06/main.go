package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day06"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	coordinates := day06.Coordinates(input, "\n")

	fmt.Printf("LargestSafe: %d\n", day06.LargestSafe(coordinates))
	fmt.Printf("LargestWithin: %d\n", day06.LargestWithin(coordinates, 10000))
}
