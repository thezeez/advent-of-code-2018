package main

import (
	"fmt"
	"strings"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day10"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	points := day10.GetPoints(helpers.CleanSliceOfStrings(input, "\n"))
	message, time := day10.Message(points)

	fmt.Printf("Message: %d, message:\n%s\n", time, strings.Join(message, "\n"))
}
