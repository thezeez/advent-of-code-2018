package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day03"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	claims := helpers.CleanSliceOfStrings(input, "\n")

	fmt.Printf("Overlap: %d\n", day03.Overlap(claims))
	fmt.Printf("Intact: %d\n", day03.Intact(claims))
}
