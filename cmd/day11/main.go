package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day11"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")
	serial := helpers2017.IntOrPanic(input)
	grid := day11.MakeGrid(serial)

	pos, _ := day11.MaxPower(grid, 3)
	fmt.Printf("MaxPower: %v\n", pos)

	pos, size := day11.MaxPowerGrid(grid)
	fmt.Printf("MaxPowerGrid: %v,%d\n", pos, size)
}
