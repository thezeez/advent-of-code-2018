package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day02"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	ids := helpers.CleanSliceOfStrings(input, "\n")

	fmt.Printf("Checksum: %d\n", day02.Checksum(ids))
	fmt.Printf("Common: %q\n", day02.Common(ids))
}
