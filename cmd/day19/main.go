package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day19"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	instructions := helpers.CleanSliceOfStrings(input, "\n")

	fmt.Printf("BackgroundProcess (Part 1): %d\n", day19.BackgroundProcess(instructions, false, true))
	fmt.Printf("BackgroundProcess (Part 2): %d\n", day19.BackgroundProcess(instructions, true, true))
}
