package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day23"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	nanobots := day23.NewBots(helpers.CleanSliceOfStrings(input, "\n"))

	fmt.Printf("StrongestReachable: %d\n", day23.StrongestReachable(nanobots))
	fmt.Printf("ClosestSuccess: %d\n", day23.ClosestSuccess(nanobots))
}
