package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day17"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	clay := helpers.CleanSliceOfStrings(input, "\n")

	m := day17.NewMap(clay)
	m.Flood()
	m.Print()

	fmt.Printf("All water: %d\n", m.Count(day17.KindFlow|day17.KindWater))
	fmt.Printf("Settled: %d\n", m.Count(day17.KindWater))
}
