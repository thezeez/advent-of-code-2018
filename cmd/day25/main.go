package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day25"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	constellations := helpers.CleanSliceOfStrings(input, "\n")

	fmt.Printf("HotChocolatePortal: %d\n", day25.HotChocolatePortal(constellations))
}
