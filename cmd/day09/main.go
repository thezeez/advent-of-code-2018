package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day09"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	players, marbles := day09.ParseInput(input)

	fmt.Printf("Marbles: %d\n", day09.Marbles(players, marbles))
	fmt.Printf("Marbles100: %d\n", day09.Marbles(players, marbles*100))
}
