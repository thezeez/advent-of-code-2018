package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day16"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	instructions := helpers.CleanSliceOfStrings(input, "\n")

	fmt.Printf("TripleSamples: %d\n", day16.TripleSamples(instructions))
	fmt.Printf("ExecuteProgram: %d\n", day16.ExecuteProgram(instructions))
}
