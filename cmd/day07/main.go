package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day07"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

const (
	baseTime = 60
	workers  = 5
)

func main() {

	input := helpers2017.ReadStringFromFile("input.txt")

	steps := helpers.CleanSliceOfStrings(input, "\n")

	fmt.Printf("Order: %q\n", day07.Order(day07.Steps(steps, baseTime)))
	fmt.Printf("TimeToCompletion: %d\n", day07.TimeToCompletion(day07.Steps(steps, baseTime), workers))
}
