package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day20"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	fmt.Printf("DooriestRoom: %d\n", day20.DooriestRoom(input))
	fmt.Printf("NumberOfDoors: %d\n", day20.NumberOfDoors(input, 1000))
}
