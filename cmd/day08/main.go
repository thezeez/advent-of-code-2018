package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day08"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	license := day08.ParseLicense(input)

	fmt.Printf("SumMetadata: %d\n", day08.SumMetadata(license))
	fmt.Printf("NodeValue: %d\n", day08.NodeValue(license))
}
