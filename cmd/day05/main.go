package main

import (
	"fmt"
	"strings"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day05"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	polymerChain := strings.TrimSpace(input)

	fmt.Printf("Reduction: %d\n", len(day05.Reduction(polymerChain)))
	fmt.Printf("BestCollapse: %d\n", len(day05.BestCollapse(polymerChain)))
}
