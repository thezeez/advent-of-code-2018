package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day12"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	meh := helpers.CleanSliceOfStrings(input, "\n")

	initial, growth := meh[0], meh[1:]


	fmt.Printf("PlantGrowth20: %d\n", day12.PlantGrowth(initial, growth, 20))
	fmt.Printf("PlantGrowth5e10: %d\n", day12.PlantGrowth(initial, growth, 5e10))
}
