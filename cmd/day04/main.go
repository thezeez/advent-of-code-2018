package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day04"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	cycle := helpers.CleanSliceOfStrings(input, "\n")
	schedule := day04.SleepSchedule(cycle)

	fmt.Printf("X: %d\n", day04.MostAsleep(schedule))
	fmt.Printf("Y: %d\n", day04.MostCommonMinute(schedule))
}
