package main

import (
	"fmt"

	helpers2017 "bitbucket.org/thezeez/advent-of-code-2017/helpers"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/day01"
	"bitbucket.org/thezeez/advent-of-code-2018/pkg/helpers"
)

func main() {
	input := helpers2017.ReadStringFromFile("input.txt")

	changes := helpers.ToSliceOfInt(input, "\n")

	fmt.Printf("FrequencyChange: %d\n", day01.FrequencyChange(changes))
	fmt.Printf("DoubleFrequencyChange: %d\n", day01.DoubleFrequencyChange(changes))
}
