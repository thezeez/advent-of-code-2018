package helpers

import (
	"strconv"
	"strings"
)

func Abs(x int) int {
	if x >= 0 {
		return x
	}
	return -x
}

func IntOrPanic(input string) int {
	i, err := strconv.ParseInt(strings.TrimSpace(input), 10, 64)
	if err != nil {
		panic(err)
	}
	return int(i)
}
