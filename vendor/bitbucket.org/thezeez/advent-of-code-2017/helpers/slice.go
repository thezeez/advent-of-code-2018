package helpers

func Contains(s []int, e int) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func Index(s []int, e int) int {
	for i, se := range s {
		if se == e {
			return i
		}
	}
	return -1
}

func Delete(s []int, e int) []int {
	i := Index(s, e)
	if i >= 0 {
		if len(s) > 1 {
			return append(s[:i], s[i+1:]...)
		}
		return []int{}
	}
	return s
}

func Copy(s []int) []int {
	c := make([]int, len(s))
	copy(c, s)
	return c
}

func MaxIndex(input []int) (index int) {
	max := 0
	for i, data := range input {
		if i == 0 || data > max {
			index = i
			max = data
		}
	}
	return
}

func Equal(a []int, b []int) bool {
	if &a == &b {
		return true
	}
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if b[i] != v {
			return false
		}
	}
	return true
}
